import { create } from "zustand";
import { immer } from "zustand/middleware/immer";
import { AddressFormTypes } from "@/utils/type";

type State = {
  selectedLocations: AddressFormTypes[];
  tempEditData: AddressFormTypes | null;
};

type Actions = {
  addLocation: (data: AddressFormTypes) => void;
  deleteLocation: (id: number) => void;
  editLocation: (data: AddressFormTypes) => void;
  setTempEditData: (data: AddressFormTypes | null) => void;
};

export const useLocationStore = create<State & Actions>()(
  immer((set) => ({
    selectedLocations: [],
    tempEditData: null,
    addLocation: (data: AddressFormTypes) =>
      set((state) => ({
        selectedLocations: [...state.selectedLocations, data],
      })),
    setTempEditData: (data: AddressFormTypes | null) =>
      set(() => ({
        tempEditData: data,
      })),
    editLocation: (newLocation: AddressFormTypes) => {
      set((state) => ({
        selectedLocations: state.selectedLocations.map((location) =>
          location.id === newLocation.id ? newLocation : location
        ),
      }));
    },
    deleteLocation: (id: number) =>
      set((state) => ({
        selectedLocations: state.selectedLocations.filter(
          (location) => location.id !== id
        ),
      })),
  }))
);
