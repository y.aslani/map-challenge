export type LocationType = {
  lat: number | string;
  lng: number | string;
};

export type AddressFormTypes = {
  id?: number;
  name: string;
  location: LocationType;
  type: string;
  logo: string;
};
