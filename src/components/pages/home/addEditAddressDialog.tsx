import React, { useState, FormEvent } from "react";
import MainButton from "@/components/general/mainButton";
import MainInput from "@/components/general/mainInput";
import MainSelect from "@/components/general/mainSelect";
import MainImageUploader from "@/components/general/mainImageUploader";
import MainMap from "@/components/general/mainMap";
import { useLocationStore } from "@/store/mapData";
import { AddressFormTypes } from "@/utils/type";

const addressTypeOptions = [
  { value: "home", label: "خانه" },
  { value: "business", label: "کار" },
];

interface Props {
  onClose: () => void;
}

const AddEditAddressDialog = ({ onClose }: Props) => {
  const { addLocation, tempEditData, setTempEditData, editLocation } =
    useLocationStore();

  const [formItems, setFormItems] = useState<AddressFormTypes>({
    name: tempEditData?.name ?? "",
    type: tempEditData?.type ?? addressTypeOptions[0].value,
    logo: tempEditData?.logo ?? "",
    location: tempEditData?.location ?? { lat: "", lng: "" },
  });

  const closeDialog = () => {
    if (tempEditData) setTempEditData(null);
    onClose();
  };

  const updateForm = (key: string, value: string | object) => {
    setFormItems((prev: AddressFormTypes) => ({ ...prev, [key]: value }));
  };

  const onSubmitForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!formItems?.location.lat) {
      //TODO show error
      return;
    }
    if (tempEditData && tempEditData.id)
      editLocation({ ...formItems, id: tempEditData.id });
    else addLocation({ ...formItems, id: Math.random() });
    closeDialog();
  };

  return (
    <form className="flex flex-col" onSubmit={onSubmitForm}>
      <MainInput
        label=" نام"
        name="name"
        value={formItems.name}
        onChangeValue={updateForm}
      />
      <MainMap
        className="w-4/5 h-40 mb-4 mr-auto"
        name="location"
        value={formItems.location}
        onChangeValue={updateForm}
        zoom={10}
      />

      <MainSelect
        label=" نوع"
        name="type"
        value={formItems.type}
        options={addressTypeOptions}
        onChangeValue={updateForm}
      />
      <MainImageUploader
        label="لوگو"
        name="logo"
        value={formItems.logo}
        onChangeValue={updateForm}
      />

      <div className="flex justify-end">
        <MainButton type="submit" className="bg-blue-600 text-white">
          {tempEditData ? "ویرایش" : "ذخیره"}
        </MainButton>
        <MainButton className="bg-gray-300 mr-2" onClick={closeDialog}>
          بستن
        </MainButton>
      </div>
    </form>
  );
};

export default AddEditAddressDialog;
