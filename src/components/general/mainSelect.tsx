import React, { ChangeEvent, ComponentProps } from "react";

interface Props extends ComponentProps<"select"> {
  label?: string;
  name: string;
  options: { value: string; label: string }[];
  onChangeValue: (key: string, value: string) => void;
}

const MainSelect = ({
  label,
  options,
  onChangeValue,
  name,
  ...props
}: Props) => {
  return (
    <div className="flex items-center justify-between mb-4">
      {label && <label className="ml-2 ">{label}:</label>}
      <select
        {...props}
        className="border border-gray-300 rounded px-3 py-2 w-4/5"
        onChange={(e: ChangeEvent<HTMLSelectElement>) =>
          onChangeValue(name, e.target.value)
        }
      >
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default MainSelect;
