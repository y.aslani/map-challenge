import React, { useRef, useEffect } from "react";
import mapboxgl, { Map, Marker } from "mapbox-gl";
import { AddressFormTypes, LocationType } from "@/utils/type";
import "mapbox-gl/dist/mapbox-gl.css";
import { useLocationStore } from "@/store/mapData";
import { createRoot } from "react-dom/client";
import MainButton from "./mainButton";
import Image from "next/image";

mapboxgl.accessToken =
  "pk.eyJ1Ijoic2xuLXllZ2FuZSIsImEiOiJjbHBmNW1tOWkwaWZqMmpwYmNnaHI4eWdrIn0.Z4SnAC3beuBYRHVit9v5Hw";

mapboxgl.setRTLTextPlugin(
  "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js",
  (error: Error): void => {}
);

const apiKey =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImYyY2VhZTY2NTdiM2M5MGViYmFhOWI2ZmVjZmJkYjk1ZWExMGFlOWVmODM0MzJjZmJhYjJiZjgxMWEwZWVmYzFjNTQ5N2UwYjI0YWY5MzliIn0.eyJhdWQiOiIyNTA5NCIsImp0aSI6ImYyY2VhZTY2NTdiM2M5MGViYmFhOWI2ZmVjZmJkYjk1ZWExMGFlOWVmODM0MzJjZmJhYjJiZjgxMWEwZWVmYzFjNTQ5N2UwYjI0YWY5MzliIiwiaWF0IjoxNzAwOTgwMjk4LCJuYmYiOjE3MDA5ODAyOTgsImV4cCI6MTcwMzQ4NTg5OCwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.WNkqb2_X47gN89VfAKzex1ukfECmeHKTagffprcbA03xqgX_NtfZ3hVM6Yahj7PJlfcdQAQqMpvIBCo9p2d_eqrsl9oV9uJ70GIvYQIERQPPpq1pUMmlMsx4JTZ2Lnj0byg2TB8CoLbdwrrxn7CmsPz5g0rV0_7UAKKry3dmiqdhWiXuRFW-mgR5VphyyizayC07HhNDr98FDzN3fE0TjmlDo7xZ84fBxBoCQC5NmRmgDSHn2_c0y0VEnoj_MwzWG4eqCwnYP2hybg7A2YjAbR7o3hpw0-IDRtxdf_sGgWRPLV8QmBugUyxWmnQDDignWvUOaxBm98_XorrT8KjDwA";

interface Props {
  viewMode?: boolean;
  zoom?: number;
  name: string;
  className?: string;
  value?: LocationType;
  onChangeValue?: (key: string, value: LocationType) => void;
  openLocationEditDialog?: () => void;
}
const MainMap = ({
  viewMode = false,
  zoom = 12,
  name,
  value,
  onChangeValue,
  className,
  openLocationEditDialog,
}: Props) => {
  const mapContainer = useRef<HTMLDivElement | null>(null);
  const map = useRef<Map | null>(null);
  const markerElement = useRef<Marker[]>([]);

  const { selectedLocations, deleteLocation, setTempEditData } =
    useLocationStore();

  useEffect(() => {
    if (map.current) return; // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current!,
      style: `https://map.ir/vector/styles/main/mapir-xyz-style.json?x-api-key=${apiKey}`,
      zoom: zoom,
      center:
        value?.lng && value.lat
          ? [+value.lng, +value.lat]
          : [51.362715, 35.73689],
      transformRequest: (url: string) => {
        return {
          url: url,
          headers: { "x-api-key": apiKey },
        };
      },
    });

    if (viewMode) return;

    map.current.on("load", () => {
      const el = document.createElement("div");
      el.className = "marker";
      //@ts-ignore
      markerElement.current[0] = new mapboxgl.Marker({ el });

      const changeMarkerPosition = () => {
        const temp: LocationType = {
          lat: map.current?.getCenter().lat.toFixed(4) ?? "",
          lng: map.current?.getCenter().lng.toFixed(4) ?? "",
        };

        markerElement.current[0]
          ?.setLngLat([+temp.lng, +temp.lat])
          ?.addTo(map.current!);

        onChangeValue?.(name, {
          lng: temp.lng,
          lat: temp.lat,
        });
      };

      changeMarkerPosition();
      map?.current?.on("drag", () => {
        changeMarkerPosition();
      });
      map?.current?.on("zoom", () => {
        changeMarkerPosition();
      });
    });
  }, []);

  useEffect(() => {
    if (
      map.current &&
      markerElement.current &&
      viewMode &&
      selectedLocations.length
    ) {
      markerElement.current.forEach((marker: any) => marker.remove());
      markerElement.current = [];

      for (let i = 0; i < selectedLocations.length; i++) {
        const tempLocation: AddressFormTypes = selectedLocations[i];
        const el = document.createElement("div");
        el.className = `marker ${tempLocation.id}`;
        el.id = tempLocation?.id?.toString() ?? "";

        const popupContainer = document.createElement("div");
        const popupElement = createRoot(popupContainer);
        const deleteSelectedLocation = () => {
          deleteLocation(tempLocation?.id ?? 0);
          markerElement.current[i].remove();
          markerElement.current.splice(i, 1);
          el.remove();
        };
        const editSelectedLocation = () => {
          setTempEditData(tempLocation);
          openLocationEditDialog?.();
        };
        popupElement.render(
          <div className="p-1">
            <div className="my-2">
              {tempLocation.logo && (
                <Image
                  src={tempLocation.logo}
                  alt={tempLocation.name}
                  width={50}
                  height={50}
                  className="rounded-full"
                />
              )}
              {tempLocation.name}
              {tempLocation.type}
            </div>
            <div>
              <MainButton
                onClick={deleteSelectedLocation}
                className="bg-red-600 ml-2 text-white"
              >
                حذف
              </MainButton>
              <MainButton
                onClick={editSelectedLocation}
                className="bg-green-600 py-1 text-white"
              >
                ویرایش
              </MainButton>
            </div>
          </div>
        );

        //@ts-ignore
        markerElement.current[i] = new mapboxgl.Marker({ el })
          .setLngLat([+tempLocation.location.lng, +tempLocation.location.lat])
          .setPopup(
            new mapboxgl.Popup({ offset: 12 }).setDOMContent(popupContainer)
          )
          .addTo(map.current);
      }
      map.current.setCenter([
        +selectedLocations[selectedLocations.length - 1].location.lng,
        +selectedLocations[selectedLocations.length - 1].location.lat,
      ]);
    }
  }, [selectedLocations]);

  return <div ref={mapContainer} className={className} />;
};

export default MainMap;
