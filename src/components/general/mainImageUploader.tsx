import Image from "next/image";
import React, { useState, DragEvent, ChangeEvent } from "react";
import MainButton from "./mainButton";

interface Props {
  name: string;
  label: string;
  className?: string;
  value?: string;
  onChangeValue: (key: string, value: string) => void;
}

const MainImageUploader = ({
  onChangeValue,
  name,
  value,
  label,
  className,
}: Props) => {
  const [isDragging, setIsDragging] = useState(false);

  const onSaveImage = (file: File | null) => {
    if (file && file.type.startsWith("image/")) {
      const generatedLink = URL.createObjectURL(file);
      onChangeValue(name, generatedLink);
    }
  };

  //add logo with drag and drop
  const handleDragEnter = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    setIsDragging(true);
  };

  const handleDragLeave = () => {
    setIsDragging(false);
  };

  const handleDrop = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    setIsDragging(false);

    const file = e.dataTransfer.files[0];
    onSaveImage(file);
  };
  //

  const handleFileInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files && e.target.files[0];
    onSaveImage(file);
  };

  return (
    <div className={`flex items-center justify-between mb-4 ${className} `}>
      {label && <label className="ml-2">{label}:</label>}
      {!value ? (
        <div
          className={`border-dashed border-2 p-4 text-center w-4/5 ${
            isDragging ? "border-blue-500" : "border-gray-300"
          }`}
          onDragOver={(e) => e.preventDefault()}
          onDragEnter={handleDragEnter}
          onDragLeave={handleDragLeave}
          onDrop={handleDrop}
        >
          <input
            id="file-input"
            type="file"
            accept="image/*"
            onChange={handleFileInputChange}
            className="hidden"
          />
          <p className="text-gray-500">تصویر را اینجا رها کنید</p>
          <p className="text-gray-500">یا</p>
          <label htmlFor="file-input" className="cursor-pointer text-blue-500">
            انتخاب
          </label>
        </div>
      ) : (
        <div className="w-4/5 flex justify-center relative">
          <Image
            src={value}
            alt={name}
            height={150}
            width={0}
            className="w-full h-40 object-contain"
          />
          <MainButton
            onClick={() => {
              onChangeValue(name, "");
            }}
            className="absolute top-1 left-3 px-1 py-1 text-xl font-bold text-red-600"
          >
            ×
          </MainButton>
        </div>
      )}
    </div>
  );
};

export default MainImageUploader;
