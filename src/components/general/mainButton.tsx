"use client";

import React, { ComponentProps } from "react";

interface Props extends ComponentProps<"button"> {
  icon?: string;
}

const MainButton = ({
  className,
  onClick,
  type = "button",
  children,
}: Props) => {
  return (
    <button
      type={type}
      className={`rounded-md py-2 px-4 ${className}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default MainButton;
