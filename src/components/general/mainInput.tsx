import React, { ChangeEvent, ComponentProps } from "react";

interface Props extends ComponentProps<"input"> {
  label?: string;
  name: string;
  onChangeValue: (key: string, value: string) => void;
}

const MainInput = ({
  label,
  className,
  onChangeValue,
  value,
  name,
  ...props
}: Props) => {
  return (
    <div className={`flex items-center justify-between mb-4 ${className} `}>
      {label && <label className="ml-2">{label}:</label>}
      <input
        {...props}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          onChangeValue(name, e.target.value)
        }
        value={value}
        className="border border-gray-300 rounded px-3 py-2 w-4/5"
      />
    </div>
  );
};

export default MainInput;
