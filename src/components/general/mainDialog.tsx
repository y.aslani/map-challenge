import React, { ReactNode } from "react";
import { createPortal } from "react-dom";

interface Props {
  content: ReactNode;
}

const MainDialog = ({ content }: Props) => {
  return createPortal(
    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-auto bg-black bg-opacity-50">
      <div className="relative bg-white rounded-lg p-4 max-w-md w-full">
        {content}
      </div>
    </div>,
    document.body //TODO
  );
};

export default MainDialog;
