"use client";

import React, { useState } from "react";
import MainButton from "@/components/general/mainButton";
import MainMap from "@/components/general/mainMap";
import MainDialog from "@/components/general/mainDialog";
import AddEditAddressDialog from "@/components/pages/home/addEditAddressDialog";

export default function Home() {
  const [openAddressDialog, setOpenAddressDialog] = useState<boolean>(false);
  return (
    <main className="h-screen overflow-hidden relative	">
      <MainMap
        className="w-full h-full"
        name="location"
        viewMode={true}
        openLocationEditDialog={() => setOpenAddressDialog(true)}
      />
      <MainButton
        onClick={() => setOpenAddressDialog(true)}
        className="absolute bottom-2 right-2 bg-blue-600 text-white"
      >
        افزودن آدرس جدید
      </MainButton>

      {openAddressDialog && (
        <MainDialog
          content={
            <AddEditAddressDialog onClose={() => setOpenAddressDialog(false)} />
          }
        />
      )}
    </main>
  );
}
